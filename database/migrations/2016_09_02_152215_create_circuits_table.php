<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCircuitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circuits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('circuit_number');
            $table->integer('circuit_area')->unsigned();
            $table->string('floor_type');
            $table->integer('floor_construction_id')->unsigned();
            $table->integer('fixing_method_id')->unsigned();
            $table->string('pipe_length');
            $table->string('pipe_spacing');
            $table->string('pipe_size');
            $table->integer('room_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('circuits');
    }
}
