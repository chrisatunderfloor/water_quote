<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quoteRef');
            $table->string('creationDate');
            $table->integer('ownerId')->unsigned();
            $table->integer('customerId')->unsigned();
            $table->string('notes');
            $table->string('heatSource');
            $table->decimal('discountPercentage', 4, 2)->unsigned();
            $table->string('status');
            $table->integer('parentId')->unsigned();
            $table->decimal('totalValue', 7, 2)->unsigned();
            $table->decimal('discountValue', 7, 2)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quotes');
    }
}
