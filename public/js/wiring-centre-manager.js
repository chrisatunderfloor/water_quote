var wiringCentres = {};

$(document).ready(function() {

    $('#add-wiring-centre-button').click(function(e) {
        e.preventDefault();
        var selectedCentreCode = $('#centres-dropdown').val();
        var selectedCentreText = $('#centres-dropdown option:selected').text();
        var quantity = Number($('#quantity').val());

        if (validate_data(selectedCentreCode, quantity)) {

            // Add the users selection to the master holding array
            update_master_object(selectedCentreCode, selectedCentreText, quantity);

            // Create the hidden fields in the main form
            create_hidden_fields();

            // Build the table from the master array to show the user
            build_table();
        }

        // Reset input fields
        $('#centres-dropdown option:eq(0)').attr('selected', 'selected');
        $('#quantity').val('');
    });

    if (!$.isEmptyObject(wiringCentres)) {
        // Create the hidden fields in the main form
        create_hidden_fields();
        // Build the table from the master array to show the user
        build_table();
    }
});

/**
 * Updates the master wiring centres array
 * 
 * It either adds a new entry if the wiring centre doesn't already 
 * exist in the array, or increments the quantity vale of the wiring 
 * centre if it does.
 * 
 * @returns {undefined}
 */
function update_master_object(code, centreName, quantity) {
    if (code in wiringCentres) {
        wiringCentres[code].qty += quantity;
    }
    else {
        wiringCentres[code] = {
            name: centreName,
            qty: quantity
        }
    }
}

/**
 * Creates hidden fields to represent the wiring centres
 * 
 * Uses the master array to build the hidden fields
 */
function create_hidden_fields() {

    $hiddenFieldsContainer = $('#hidden-fields');
    $hiddenFieldsContainer.empty();

    for (var code in wiringCentres) {
        // Create hidden form field and attach to form
        var input = $('<input type="hidden" name="wiring-centres[' + code + ']" value="' + wiringCentres[code].qty + '" />');
        $hiddenFieldsContainer.append(input);
    }

}

/**
 * Build the wiring centre table
 * 
 * Uses the master array as the template and builds the wiring centre table to display the data.
 */
function build_table() {
    var $table = $('#centres-table');
    $table.closest('section').css('display', 'block');

    // Destroy all existing rows with TDs in them
    $table.find('tr:has(td)').remove();

    if ($.isEmptyObject(wiringCentres)) {
        $table.closest('section').css('display', 'none');
    }
    else {
        for (var code in wiringCentres) {
            // Create new table row to show the added wiring centre
            var $row = $('<tr><td>' + wiringCentres[code].name + '</td><td>' + wiringCentres[code].qty + '</td><td><a class="waticon-delete" data-id="' + code + '"></a></td></tr>');
            $row.insertAfter($table.find('tr').last());
        }
        // Attach events to delete buttons
        $('a', $table).click(delete_wiring_centre);
    }
}

/**
 * Deletes a wiring centre from everywhere
 * 
 * Removes the wiring centre from the master array then rebuildsa the hidden fields and the table contents
 */
function delete_wiring_centre() {

    // Remove the selected wiring centre from the main object
    var code = $(this).data('id');
    delete wiringCentres[code];

    // Create the hidden fields in the main form
    create_hidden_fields();

    // Build the table from the master array to show the user
    build_table();
}

/**
 * Validate the supplied data
 * 
 * Makes sure a wiring centre has been selected and that a valid quantity value has been entered
 * 
 * @param str centres   The selected value from the centres dropdown box
 * @param str qty       The value entered in the quantity box
 */
function validate_data(centres, qty) {
    var notice = [];

    if (centres == '') {
        notice.push('<p>Choose which wiring centre you want to add</p>');
    }
    // Regex to test for whole numbers only
    var rx = new RegExp(/^\d+$/);

    if (qty == '') {
        notice.push('<p>How many wiring centres are you adding?</p>');
    }
    else if (!rx.test(qty)) {
        notice.push('<p>You can only enter whole numbers into the quantity field.</p>');
    }
    // Output a failure message if there is one
    if (notice.length > 0) {
        for (var i = 0; i < notice.length; i++) {
            $('<div class="notice danger">' + notice[i] + '</div>').insertAfter($('#mini-form-container'));
        }
        return false;
    }
    return true;
}