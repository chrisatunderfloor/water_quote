
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: 'body'
});



// Used to determine whether the toggle animation should run.
// Buttons inside a toggle element set this value
var toggle = true;

$(document).ready(function() {

    $('.currency').wrap('<span class="currency-container"></span>').before('<span class="symbol">£</span>');

    // Nested table sub row toggling
    $('.table.nested').closest('tr').prev().find('td').not('.no-click').click(function() {
        $(this).closest('tr').next('tr').toggleClass('hidden');
    });

    // Fades out any temporary notices
    fade_out_notices(3000);

    // Reloads the quote review page so the new quote status
    // can be used to hide the buttons and shit
    $('#print-button').click(function() {
        setTimeout(function() {
            window.location.reload();
        }, 2000);
    });

    $('.parts-list td[data-id]').click(edit_part_values_on_click);

    $('.panel-heading.toggle a').click(function() {
        // disable toggling animation if the button is clicked.
        toggle = false;
    });

    $('.panel-heading.toggle').click(function(e) {
        // we use toggle to disable the animation if a button inside the element is clicked.
        if (toggle) {
            var elems;
            if (e.altKey == true) {
                if ($(this).data('toggle-group') != "undefined") {
                    elems = $('.panel-heading.toggle[data-toggle-group="' + $(this).data('toggle-group') + '"]');
                }
                else {
                    elems = $('.panel-heading.toggle')
                }
            }
            else {
                elems = $(this);
            }
            elems.toggleClass('active');
            elems.next('div').toggleClass('closed');
        }
        toggle = true;
    });

    $.each($('.panel-heading.toggle'), function() {
        var height = $(this).next('div').height();
        var $next = $(this).next('div');
        if ($next.css('max-height') == "none") {
            $(this).next('div').css('max-height', height + 200);
        }
    });

    $('.thermostat-upgrade').focus(function() {
        var $self = $(this);
        $self.data('oldValue', $self.val());
    }).blur(function() {
        var $self = $(this);
        // If they have only entered a whole number we are good to go
        if ($self.val().search(/^[0-9]+\.?[0-9]*$/) != -1) {
            var url = '/water_quote/quote_review/ajax_update_thermostat_choice_entry/quoteId/' + $self.data('quote-id');
            // Update the altered value via AJAX
            update_single_value_ajax(url, null, $self.data('id'), $self.attr('name'), $self.val());

            $self.addClass('confirmed');
            setTimeout(function() {
                $self.removeClass('confirmed');
            }, 2000);
        }
        else {
            $self.val($self.data('oldValue'));
        }
    });


});

var edit_part_values_on_click = function() {
    var $self = $(this);
    $self.off('click');
    var width = $self.width();
    width -= 6;
    // Save a copy of the original data
    $self.data('oldValue', $self.text());
    // create text input field containing elem value
    $self.html('<input type="text" name="' + $self.data('field') + '" value="' + $self.text() + '" class="form-control editable-control input-sm" style="width:' + width + 'px;">');
    // give the input focus and add the blur event handler
    $('input', this)
            .focus()
            .blur(function() {
                var $self = $(this);  // Remember, this is a new scope, different from above
                // If they have only entered a whole number we are good to go
                if ($self.val().search(/^[0-9]+\.?[0-9]*$/) != -1) {
                    var pData = $self.parent().data();
                    var url = '/water_quote/quote_review/ajax_update_parts_list_entry/quoteId/' + pData.quoteid;
                    // Update the altered value via AJAX
                    update_single_value_ajax(url, pData.type, pData.id, pData.field, $self.val());

                    setTimeout(function() {
                        window.location.reload();
                    }, 100);
                }
                else {
                    $self.parent().html($self.parent().data('oldValue'));
                    window.location.reload();
                }

            });
}

// Initilise tool tips
$(function() {
    $('[data-toggle="tooltip"]').tooltip({container: 'body'})
})

var fadeIntervalHandle;

// Fades out any temporary notices
var fade_out_notices = function(time) {
    clearTimeout(fadeIntervalHandle);
    fadeIntervalHandle = setTimeout(function() {
        $('.notice-fading').fadeOut(400, function() {
            $(this).addClass('hidden').css('display', 'block');
        });
    }, time);
}

/**
 * Triggers AJAX call to water_quote/ajax/update_single_value
 * 
 * @param string type
 * @param int id
 * @param string field
 * @param mixed value
 */
var update_single_value_ajax = function(url, type, id, field, value) {

    url = url == undefined ? '/water_quote/ajax/update_single_value' : url;

    $.ajax({
        url: url,
        type: 'POST',
        data: {
            type: type,
            id: id,
            field: field,
            value: value
        },
        success: function(data, status, jqXHR) {

        },
        error: function(jqXHR, status, errorThrown) {
            alert("There was an error\n\n" + errorThrown);
        }
    });
}

window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');
require('vue-resource');

/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

Vue.http.interceptors.push((request, next) => {
    request.headers['X-CSRF-TOKEN'] = Laravel.csrfToken;

    next();
});

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from "laravel-echo"

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });

// Global namespace
 
(function($) {

    ButtonWidget = function(options) {

        // PRIVATE PROPERTIES
        var obj = this;
        var buttons = {};
        var settings = {
            id: '',
            functions: {},
            appPath: '/water_quote/',
            classPrefix: 'waticon-',
            inlineClasses: {
                qty: {
                    step: 1,
                    className: 'quantity-small'
                },
                'unit-price': {
                    step: 0.01,
                    className: 'price'
                }
            },
            appendTo: false,
            hoverTrigger: false
        };

        // PUBLIC PROPERTIES
        this.widget = {};

        //----------------------------//
        //      PRIVATE METHODS       //
        //----------------------------//

        /**
         * Initialise the widget class
         * 
         * Updates the options with anything passed in. Checks to make sure we 
         * have an appendTo element.  If no hoverTrigger is supplied, it uses 
         * the appendTo element as the hover trigger.  Once we're happy it 
         * creates an instance of the widget.
         * 
         * @access private
         * @param obj options       A hash containing settings for the object
         */
        function init(options) {

            // Extend our private settings property with the object supplied during instantiation
            $.extend(settings, options);

            if (settings.appendTo === false) {
                throw new Error('You must supply a reference to the element you want to append the widget to');
            }

            // If no hoverTrigger is supplied use the current appendTo element as the trigger
            settings.hoverTrigger = settings.hoverTrigger.length > 0 ? settings.hoverTrigger : settings.appendTo;

            // Create our main widget DOM object
            createWidget();
        }

        /**
         * Creates a new widget instance
         * 
         * Creates the widget DOM element, appends it to the supplied DOM 
         * element and sets the hover state to the supplied hoverTrigger DOM element.
         * 
         * @access private
         */
        function createWidget() {
            obj.widget = $('<div class="function-widget"></div>');

            // Add the buttons
            for (var func in settings.functions) {
                obj.addButton(func, settings.functions[func]);
            }

            // Append the new widget to the supplied DOM element
            $(settings.appendTo).append(obj.widget);

            // Add the hover trigger event to show and hide the widget
            $(settings.hoverTrigger).hover(
                    function() {
                        $(this).data('widget').show();
                    },
                    function() {
                        $(this).data('widget').hide();
                    });
        }

        /**
         * Add a new button to the widget
         * 
         * Creates an <code><a></code> tag to represent a button
         * 
         * @access private
         * @param str href          The link href value
         * @param str title         The link title value
         * @param str className     The link class name, this will be prefixed 
         *                          by whatever is in the settings of this object
         */
        function addButtonClickEvent($button, buttonType) {

            if (buttonType == 'edit_inline') {
                $button.on('click', convertValuesToInputFields);
            }
            else if (buttonType == 'cancel') {
                $button.on('click', removeInputFields);
            }
            else if (buttonType == 'submit') {
                $button.on('click', submitEdits);
            }
        }

        /**
         * Submit the containing form after inline edits are complete
         * 
         * @access private
         * @param obj event
         */
        function submitEdits(event) {
            event.preventDefault();
            $(event.target).closest('form').submit();
        }

        /**
         * Converts specific table cell values to input fields
         * 
         * The class identifiers for the table cells are in the 
         * settings.inlineClasses object.  Once the cells have been identified 
         * the existing text values are saved to the parent widget data 
         * parameters, before input fields are created to allow editing.
         * 
         * The edit button in the widget is removed and submit and cancel 
         * buttons are added.
         * 
         * @access private
         * @param obj event
         */
        function convertValuesToInputFields(event) {
            event.preventDefault();

            var $widget = $(obj.widget);

            // Get some reusable handles to relevant elements
            var $parentRow = $widget.closest('tr');

            // Convert all inlineClasses elements to input fields
            for (var cls in settings.inlineClasses) {

                // Get some useful handles set up
                var clsObj = settings.inlineClasses[cls];
                $field = $('.' + cls, $parentRow);

                // Save the original value
                $widget.data(cls, $field.text());

                // Convert the table cell content to an input field
                $field.html('<input type="number" step="' + clsObj.step + '" min="0" name="' + settings.id + '[' + cls + ']" class="form-control ' + clsObj.className + '" value="' + $field.text() + '" />');
            }

            // Change the available buttons based on the current action
            obj.addButton('submit');
            obj.addButton('cancel');
            obj.removeButton('edit_inline');
        }

        /**
         * Removes the input fields from the table cells
         * 
         * The table cells with class identifiers, found in the 
         * settings.inlineClasses object, have their input fields removed and 
         * their original values replaced (which are stored in the data 
         * parameters of the parent widget).
         * 
         * @access private
         * @param obj event
         */
        function removeInputFields(event) {
            event.preventDefault();

            var $widget = $(obj.widget);

            // Get some reusable handles to relevant elements
            var $parentRow = $widget.closest('tr');

            // Convert all inlineClasses elements to input fields
            for (var cls in settings.inlineClasses) {

                // Get some useful handles set up
                var clsObj = settings.inlineClasses[cls];
                $field = $('.' + cls, $parentRow);

                // Change the table cell contents back to it's original value
                $field.html($widget.data(cls));
            }

            // Change the available buttons based on the current action
            obj.removeButton('submit');
            obj.removeButton('cancel');
            obj.addButton('edit_inline');
        }

        function toProperCase(str)
        {
            var noCaps = ['of', 'a', 'the', 'and', 'an', 'am', 'or', 'nor', 'but', 'is', 'if', 'then',
                'else', 'when', 'at', 'from', 'by', 'on', 'off', 'for', 'in', 'out', 'to', 'into', 'with'];
            return str.replace(/\w\S*/g, function(txt, offset) {
                if (offset != 0 && noCaps.indexOf(txt.toLowerCase()) != -1) {
                    return txt.toLowerCase();
                }
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }

        //----------------------------//
        //      PUBLIC METHODS        //
        //----------------------------//

        /**
         * Adds a button to the current widget
         * 
         * @access public
         * @param str objectType    This is used as the controller part of the link href
         * @param str func          This is the type of operation we want to carry out
         */
        this.addButton = function(func, objectType) {
            
            objectType = typeof objectType === 'undefined' ? 'novalue' : objectType;
            var href = settings.appPath + objectType + '/' + func + '/' + settings.id;
            var className = func.split('_')[0];
            var title = toProperCase(func.replace('_', " "));
            var $button = $('<a href="' + href + '" title="' + title + '" class="' + settings.classPrefix + className + '" />');

            addButtonClickEvent($button, func);

            obj.widget.append($button);
            buttons[func] = $button;
        };

        /**
         * Removes a button from the widget
         * 
         * @access public
         * @param str buttonType    String representing the button type i.e. edit, add, view etc
         */
        this.removeButton = function(buttonType) {
            if (typeof buttons[buttonType] === 'undefined') {
                console.log('There doesn\'t seem to be a ' + buttonType + ' button in the widget?');
                return false;
            }
            $(buttons[buttonType]).remove();
        };

        /**
         * Displays the widget on screen by add a 'show' class
         * 
         * @access public
         */
        this.show = function() {
            obj.widget.addClass('show');
        }

        /**
         * Hides the widget by removing the 'show' class
         * 
         * @access public
         */
        this.hide = function() {
            obj.widget.removeClass('show');
        }

        //----------------------------------//
        //      CLASS INITIALISATION        //
        //----------------------------------//

        // Lets get this party started
        try {
            init(options);
        }
        catch (e) {
            console.log(e);
        }
    }

})(jQuery);

$(document).ready(function() {
    /**
     * Add edit widget to DOM elements with a data-functions attribute
     */
    $('[data-function]:not(:has(th))').each(function() {
        // Get a handle to 'this' to save function calls
        var $self = $(this);

        // Grab all values from the data attributes and get them in a usable form
        attrs = gatherWidgetDetails($self);

        // Work out what the appendTo should be
        $appendTo = $self[0].tagName == 'TR' ? $self.find('td').last() : $self;

        // Get the widget and save it against the current DOM element
        $self.data('widget', new ButtonWidget({
            id: attrs.id,
            functions: attrs.functions,
            appendTo: $appendTo,
            hoverTrigger: $self
        }));

    });
});

var gatherWidgetDetails = function($elem) {
    var results = {};

    var functions = $elem.data('function').replace(/ /g, "").split(',');
    var controllers = $elem.data('controller').replace(/ /g, "").split(',');

    // Create the functions object from the stuff we grabbed from the data attributes
    results.functions = createFunctionObject(functions, controllers);
    results.id = $elem.data('id');

    return results;
}

var createFunctionObject = function(funcs, controllers) {
    functions = {};
    $.each(funcs, function(i, func) {
        var x = i;
        do {
            type = controllers[x];
            x--;
        }
        while (type == undefined)

        var funcValues = func.split('_')[0];

        functions[func] = type;
    });
    return functions;
}
//# sourceMappingURL=app.js.map
