require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "../../../public/css"
sass_dir = "."
images_dir = "../../../public/img"
javascripts_dir = "../../../public/js"

# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed

# Css map output
sass_options = {:sourcemap => true}