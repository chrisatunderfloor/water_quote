
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: 'body'
});



// Used to determine whether the toggle animation should run.
// Buttons inside a toggle element set this value
var toggle = true;

$(document).ready(function() {

    $('.currency').wrap('<span class="currency-container"></span>').before('<span class="symbol">£</span>');

    // Nested table sub row toggling
    $('.table.nested').closest('tr').prev().find('td').not('.no-click').click(function() {
        $(this).closest('tr').next('tr').toggleClass('hidden');
    });

    // Fades out any temporary notices
    fade_out_notices(3000);

    // Reloads the quote review page so the new quote status
    // can be used to hide the buttons and shit
    $('#print-button').click(function() {
        setTimeout(function() {
            window.location.reload();
        }, 2000);
    });

    $('.parts-list td[data-id]').click(edit_part_values_on_click);

    $('.panel-heading.toggle a').click(function() {
        // disable toggling animation if the button is clicked.
        toggle = false;
    });

    $('.panel-heading.toggle').click(function(e) {
        // we use toggle to disable the animation if a button inside the element is clicked.
        if (toggle) {
            var elems;
            if (e.altKey == true) {
                if ($(this).data('toggle-group') != "undefined") {
                    elems = $('.panel-heading.toggle[data-toggle-group="' + $(this).data('toggle-group') + '"]');
                }
                else {
                    elems = $('.panel-heading.toggle')
                }
            }
            else {
                elems = $(this);
            }
            elems.toggleClass('active');
            elems.next('div').toggleClass('closed');
        }
        toggle = true;
    });

    $.each($('.panel-heading.toggle'), function() {
        var height = $(this).next('div').height();
        var $next = $(this).next('div');
        if ($next.css('max-height') == "none") {
            $(this).next('div').css('max-height', height + 200);
        }
    });

    $('.thermostat-upgrade').focus(function() {
        var $self = $(this);
        $self.data('oldValue', $self.val());
    }).blur(function() {
        var $self = $(this);
        // If they have only entered a whole number we are good to go
        if ($self.val().search(/^[0-9]+\.?[0-9]*$/) != -1) {
            var url = '/water_quote/quote_review/ajax_update_thermostat_choice_entry/quoteId/' + $self.data('quote-id');
            // Update the altered value via AJAX
            update_single_value_ajax(url, null, $self.data('id'), $self.attr('name'), $self.val());

            $self.addClass('confirmed');
            setTimeout(function() {
                $self.removeClass('confirmed');
            }, 2000);
        }
        else {
            $self.val($self.data('oldValue'));
        }
    });


});

var edit_part_values_on_click = function() {
    var $self = $(this);
    $self.off('click');
    var width = $self.width();
    width -= 6;
    // Save a copy of the original data
    $self.data('oldValue', $self.text());
    // create text input field containing elem value
    $self.html('<input type="text" name="' + $self.data('field') + '" value="' + $self.text() + '" class="form-control editable-control input-sm" style="width:' + width + 'px;">');
    // give the input focus and add the blur event handler
    $('input', this)
            .focus()
            .blur(function() {
                var $self = $(this);  // Remember, this is a new scope, different from above
                // If they have only entered a whole number we are good to go
                if ($self.val().search(/^[0-9]+\.?[0-9]*$/) != -1) {
                    var pData = $self.parent().data();
                    var url = '/water_quote/quote_review/ajax_update_parts_list_entry/quoteId/' + pData.quoteid;
                    // Update the altered value via AJAX
                    update_single_value_ajax(url, pData.type, pData.id, pData.field, $self.val());

                    setTimeout(function() {
                        window.location.reload();
                    }, 100);
                }
                else {
                    $self.parent().html($self.parent().data('oldValue'));
                    window.location.reload();
                }

            });
}

// Initilise tool tips
$(function() {
    $('[data-toggle="tooltip"]').tooltip({container: 'body'})
})

var fadeIntervalHandle;

// Fades out any temporary notices
var fade_out_notices = function(time) {
    clearTimeout(fadeIntervalHandle);
    fadeIntervalHandle = setTimeout(function() {
        $('.notice-fading').fadeOut(400, function() {
            $(this).addClass('hidden').css('display', 'block');
        });
    }, time);
}

/**
 * Triggers AJAX call to water_quote/ajax/update_single_value
 * 
 * @param string type
 * @param int id
 * @param string field
 * @param mixed value
 */
var update_single_value_ajax = function(url, type, id, field, value) {

    url = url == undefined ? '/water_quote/ajax/update_single_value' : url;

    $.ajax({
        url: url,
        type: 'POST',
        data: {
            type: type,
            id: id,
            field: field,
            value: value
        },
        success: function(data, status, jqXHR) {

        },
        error: function(jqXHR, status, errorThrown) {
            alert("There was an error\n\n" + errorThrown);
        }
    });
}