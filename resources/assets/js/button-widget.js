// Global namespace
 
(function($) {

    ButtonWidget = function(options) {

        // PRIVATE PROPERTIES
        var obj = this;
        var buttons = {};
        var settings = {
            id: '',
            functions: {},
            appPath: '/water_quote/',
            classPrefix: 'waticon-',
            inlineClasses: {
                qty: {
                    step: 1,
                    className: 'quantity-small'
                },
                'unit-price': {
                    step: 0.01,
                    className: 'price'
                }
            },
            appendTo: false,
            hoverTrigger: false
        };

        // PUBLIC PROPERTIES
        this.widget = {};

        //----------------------------//
        //      PRIVATE METHODS       //
        //----------------------------//

        /**
         * Initialise the widget class
         * 
         * Updates the options with anything passed in. Checks to make sure we 
         * have an appendTo element.  If no hoverTrigger is supplied, it uses 
         * the appendTo element as the hover trigger.  Once we're happy it 
         * creates an instance of the widget.
         * 
         * @access private
         * @param obj options       A hash containing settings for the object
         */
        function init(options) {

            // Extend our private settings property with the object supplied during instantiation
            $.extend(settings, options);

            if (settings.appendTo === false) {
                throw new Error('You must supply a reference to the element you want to append the widget to');
            }

            // If no hoverTrigger is supplied use the current appendTo element as the trigger
            settings.hoverTrigger = settings.hoverTrigger.length > 0 ? settings.hoverTrigger : settings.appendTo;

            // Create our main widget DOM object
            createWidget();
        }

        /**
         * Creates a new widget instance
         * 
         * Creates the widget DOM element, appends it to the supplied DOM 
         * element and sets the hover state to the supplied hoverTrigger DOM element.
         * 
         * @access private
         */
        function createWidget() {
            obj.widget = $('<div class="function-widget"></div>');

            // Add the buttons
            for (var func in settings.functions) {
                obj.addButton(func, settings.functions[func]);
            }

            // Append the new widget to the supplied DOM element
            $(settings.appendTo).append(obj.widget);

            // Add the hover trigger event to show and hide the widget
            $(settings.hoverTrigger).hover(
                    function() {
                        $(this).data('widget').show();
                    },
                    function() {
                        $(this).data('widget').hide();
                    });
        }

        /**
         * Add a new button to the widget
         * 
         * Creates an <code><a></code> tag to represent a button
         * 
         * @access private
         * @param str href          The link href value
         * @param str title         The link title value
         * @param str className     The link class name, this will be prefixed 
         *                          by whatever is in the settings of this object
         */
        function addButtonClickEvent($button, buttonType) {

            if (buttonType == 'edit_inline') {
                $button.on('click', convertValuesToInputFields);
            }
            else if (buttonType == 'cancel') {
                $button.on('click', removeInputFields);
            }
            else if (buttonType == 'submit') {
                $button.on('click', submitEdits);
            }
        }

        /**
         * Submit the containing form after inline edits are complete
         * 
         * @access private
         * @param obj event
         */
        function submitEdits(event) {
            event.preventDefault();
            $(event.target).closest('form').submit();
        }

        /**
         * Converts specific table cell values to input fields
         * 
         * The class identifiers for the table cells are in the 
         * settings.inlineClasses object.  Once the cells have been identified 
         * the existing text values are saved to the parent widget data 
         * parameters, before input fields are created to allow editing.
         * 
         * The edit button in the widget is removed and submit and cancel 
         * buttons are added.
         * 
         * @access private
         * @param obj event
         */
        function convertValuesToInputFields(event) {
            event.preventDefault();

            var $widget = $(obj.widget);

            // Get some reusable handles to relevant elements
            var $parentRow = $widget.closest('tr');

            // Convert all inlineClasses elements to input fields
            for (var cls in settings.inlineClasses) {

                // Get some useful handles set up
                var clsObj = settings.inlineClasses[cls];
                $field = $('.' + cls, $parentRow);

                // Save the original value
                $widget.data(cls, $field.text());

                // Convert the table cell content to an input field
                $field.html('<input type="number" step="' + clsObj.step + '" min="0" name="' + settings.id + '[' + cls + ']" class="form-control ' + clsObj.className + '" value="' + $field.text() + '" />');
            }

            // Change the available buttons based on the current action
            obj.addButton('submit');
            obj.addButton('cancel');
            obj.removeButton('edit_inline');
        }

        /**
         * Removes the input fields from the table cells
         * 
         * The table cells with class identifiers, found in the 
         * settings.inlineClasses object, have their input fields removed and 
         * their original values replaced (which are stored in the data 
         * parameters of the parent widget).
         * 
         * @access private
         * @param obj event
         */
        function removeInputFields(event) {
            event.preventDefault();

            var $widget = $(obj.widget);

            // Get some reusable handles to relevant elements
            var $parentRow = $widget.closest('tr');

            // Convert all inlineClasses elements to input fields
            for (var cls in settings.inlineClasses) {

                // Get some useful handles set up
                var clsObj = settings.inlineClasses[cls];
                $field = $('.' + cls, $parentRow);

                // Change the table cell contents back to it's original value
                $field.html($widget.data(cls));
            }

            // Change the available buttons based on the current action
            obj.removeButton('submit');
            obj.removeButton('cancel');
            obj.addButton('edit_inline');
        }

        function toProperCase(str)
        {
            var noCaps = ['of', 'a', 'the', 'and', 'an', 'am', 'or', 'nor', 'but', 'is', 'if', 'then',
                'else', 'when', 'at', 'from', 'by', 'on', 'off', 'for', 'in', 'out', 'to', 'into', 'with'];
            return str.replace(/\w\S*/g, function(txt, offset) {
                if (offset != 0 && noCaps.indexOf(txt.toLowerCase()) != -1) {
                    return txt.toLowerCase();
                }
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }

        //----------------------------//
        //      PUBLIC METHODS        //
        //----------------------------//

        /**
         * Adds a button to the current widget
         * 
         * @access public
         * @param str objectType    This is used as the controller part of the link href
         * @param str func          This is the type of operation we want to carry out
         */
        this.addButton = function(func, objectType) {
            
            objectType = typeof objectType === 'undefined' ? 'novalue' : objectType;
            var href = settings.appPath + objectType + '/' + func + '/' + settings.id;
            var className = func.split('_')[0];
            var title = toProperCase(func.replace('_', " "));
            var $button = $('<a href="' + href + '" title="' + title + '" class="' + settings.classPrefix + className + '" />');

            addButtonClickEvent($button, func);

            obj.widget.append($button);
            buttons[func] = $button;
        };

        /**
         * Removes a button from the widget
         * 
         * @access public
         * @param str buttonType    String representing the button type i.e. edit, add, view etc
         */
        this.removeButton = function(buttonType) {
            if (typeof buttons[buttonType] === 'undefined') {
                console.log('There doesn\'t seem to be a ' + buttonType + ' button in the widget?');
                return false;
            }
            $(buttons[buttonType]).remove();
        };

        /**
         * Displays the widget on screen by add a 'show' class
         * 
         * @access public
         */
        this.show = function() {
            obj.widget.addClass('show');
        }

        /**
         * Hides the widget by removing the 'show' class
         * 
         * @access public
         */
        this.hide = function() {
            obj.widget.removeClass('show');
        }

        //----------------------------------//
        //      CLASS INITIALISATION        //
        //----------------------------------//

        // Lets get this party started
        try {
            init(options);
        }
        catch (e) {
            console.log(e);
        }
    }

})(jQuery);

$(document).ready(function() {
    /**
     * Add edit widget to DOM elements with a data-functions attribute
     */
    $('[data-function]:not(:has(th))').each(function() {
        // Get a handle to 'this' to save function calls
        var $self = $(this);

        // Grab all values from the data attributes and get them in a usable form
        attrs = gatherWidgetDetails($self);

        // Work out what the appendTo should be
        $appendTo = $self[0].tagName == 'TR' ? $self.find('td').last() : $self;

        // Get the widget and save it against the current DOM element
        $self.data('widget', new ButtonWidget({
            id: attrs.id,
            functions: attrs.functions,
            appendTo: $appendTo,
            hoverTrigger: $self
        }));

    });
});

var gatherWidgetDetails = function($elem) {
    var results = {};

    var functions = $elem.data('function').replace(/ /g, "").split(',');
    var controllers = $elem.data('controller').replace(/ /g, "").split(',');

    // Create the functions object from the stuff we grabbed from the data attributes
    results.functions = createFunctionObject(functions, controllers);
    results.id = $elem.data('id');

    return results;
}

var createFunctionObject = function(funcs, controllers) {
    functions = {};
    $.each(funcs, function(i, func) {
        var x = i;
        do {
            type = controllers[x];
            x--;
        }
        while (type == undefined)

        var funcValues = func.split('_')[0];

        functions[func] = type;
    });
    return functions;
}