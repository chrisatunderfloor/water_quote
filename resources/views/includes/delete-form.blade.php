<?php
if ((strpos($this->uri->uri_string(), 'delete') !== FALSE))
{
    ?>
    <section>
        <h2>Are you sure you want to delete this record?</h2>
        <?php echo $deleteMessage; ?>
        
        <?php echo form_open($this->uri->uri_string()); ?>
        <input type="hidden" name="delete" value="1" />
        <button type="submit" class="btn btn-success"><?php echo $deleteButtonWording; ?></button>
        <a href="<?php echo $referrer; ?>" class="btn btn-danger"><?php echo $cancelButtonWording; ?></a>
        <?php echo form_close(); ?>
        
    </section>
    <?php
}
?>