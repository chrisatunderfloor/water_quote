<script type="text/javascript">
    $(function() {
        var options = {
            interactivityEnabled: true,
            animationEnabled: true,
            animationDuration: 500,
            axisY: {
                labelFontSize: 14,
                includeZero: false,
                labelFormatter: function(e) {
                    return "£" + CanvasJS.formatNumber(e.value, "#,#00");
                }
            },
            axisX: {
                labelFontSize: 14,
                labelAngle: -45,
                interval: 1,
                intervalType: "month",
                valueFormatString: "MMM",
                labelFormatter: function(e) {
                    return CanvasJS.formatDate(e.value, "MMM");
                }
            },
            data: [
            {
                color: '#E60004',
                lineThickness: 3,
                type: "spline",
                toolTipContent: "{x}<br /><strong>£{y}</strong>",
                dataPoints: [
                <?php
                foreach ($performanceFigures as $data)
                {
                    ?>
                    {
                        x: new Date(<?php echo $data->year; ?>, <?php echo sprintf("%'.02d\n", $data->month); ?>, 1),
                        y: <?php echo round($data->total, 2); ?>
                    },
                    <?php
                }
                ?>
                ]
            }
            ]
        };
        $("#performanceChart").CanvasJSChart(options);
    });
</script>