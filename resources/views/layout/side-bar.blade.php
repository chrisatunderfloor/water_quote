<nav id="sidebar">

    <ul>
        <li>
            <a href="">Dashboard</a>
        </li>
        <li>
            <a href="/customers/search">Customers</a>
        </li>
        <li>
            <a href="/quotes/my_quotes">My Quotes</a>
        </li>
        <li>
            <a href="/quotes/search">Quote Search</a>
        </li>
    </ul>

    <div id="version">
        <p>Water Quoting Tool v2.0</p>
        <p>Last updated : 29-9-15</p>
        <a href="mailto:webteam@theunderfloorheatingstore.com" class="contact-link">Contact support</a>
        <p class="copyright">Copyright &copy; 2015<br />The Underfloor Heating Store</p>
    </div>

</nav>