<nav id="header-nav">

    <p class="app-name">Water Quoting Tool <small>(logged in as)</small></p>

    <ul>

            <li><a href="/auth">manage users</a></li>
            <li><a href="/auth/create_user">add user</a></li>
            <li><a href="/auth/create_group">add group</a></li>

        <li><a href="/dashboard">dashboard</a></li>
        <li><a href="mailto:webteam@theunderfloorheatingstore.com">help</a></li>
        <li><a href="/auth/logout">logout</a></li>
    </ul>

    <form>
        <input type="text" name="search" placeholder="Search">
    </form>

</nav>