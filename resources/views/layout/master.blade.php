<!DOCTYPE html>
<html lang="en">
<head>

	@section('title','')
	@section('meta','')
	@section('keywords','')

	<title>@yield('title')</title> 
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        
    <meta name="robots" content="noindex, nofollow">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="@yield('keywords')"/>
	<meta name="description" content="@yield('meta')" />

	<meta name="viewport" content="initial-scale=1.0, width=device-width" />

	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/images/general/favicon.ico" type="image/x-icon">

	<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
	<link rel="stylesheet" type="text/css" media="print" href="css/print.css">

</head>
<body class="@yield('body_class')">

	@include('layout.header')
	@include('layout.side-bar')

	<div class="main-container">
		@yield('content')
	</div>

	@include('layout.footer')

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script src="js/button-widget.js"></script>
	<script src="js/app.js"></script>

	@yield('scripts')

</body>
</head>