@extends('layout.master');

@section('scripts')
    <script src="js/jquery.canvasjs.min.js"></script>
@endsection

<h1>Dashboard</h1>

<section>
    <div class="inline-block">
        <h2>Monthly Performance</h2>
        <div class="summary-panel">
            <table>
                <tr>
                    <th>Rank</th>
                    <td><?php echo $monthlyQuoteSummary['rank']; ?></td>
                </tr>
                <tr>
                    <th>Number of quotes</th>
                    <td><?php echo $monthlyQuoteSummary['num_quotes']; ?></td>
                </tr>
                <tr>
                    <th>Average quote value</th>
                    <td>&pound;<?php echo number_format($monthlyQuoteSummary['average_value'], 2); ?></td>
                </tr>
                <tr>
                    <th>Total quote value</th>
                    <td>&pound;<?php echo number_format($monthlyQuoteSummary['total_value'], 2); ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="inline-block">
        <h2>Total Performance</h2>
        <div class="summary-panel">
            <table>
                <tr>
                    <th>Rank</th>
                    <td><?php echo $totalQuoteSummary['rank']; ?></td>
                </tr>
                <tr>
                    <th>Number of quotes</th>
                    <td><?php echo $totalQuoteSummary['num_quotes']; ?></td>
                </tr>
                <tr>
                    <th>Average quote value</th>
                    <td>&pound;<?php echo number_format($totalQuoteSummary['average_value'], 2); ?></td>
                </tr>
                <tr>
                    <th>Total quote value</th>
                    <td>&pound;<?php echo number_format($totalQuoteSummary['total_value'], 2); ?></td>
                </tr>
            </table>
        </div>
    </div>
</section>

<section>
    <div class="inline-block">
        <h2>Average quote value</h2>
        <div id="performanceChart"></div>
    </div>
</section>

<section>
    <h2>Latest Quotes</h2>
    <table class="zebra">
        <tr>
            <th>Quote ref</th>
            <th>First name</th>
            <th>Surname</th>
            <th>Company</th>
            <th>Date created</th>
            <th>Value</th>
        </tr>
        <?php
        foreach ($quoteDetails as $details)
        {
            ?>
            <tr data-function="edit,delete_quote" data-controller="quotes,dashboard" data-id="<?php echo $details->id; ?>">
                <td><?php echo $details->quoteRef; ?></td>
                <td><?php echo $details->forename; ?></td>
                <td><?php echo $details->surname; ?></td>
                <td><?php echo $details->company; ?></td>
                <td><?php echo $details->creationDate; ?></td>
                <td>&pound;<?php echo number_format($details->totalValue, 2); ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
</section>

<section>
    <h2>Latest Customers</h2>
    <table class="zebra">
        <tr>
            <th>OrderWise Ref</th>
            <th>First name</th>
            <th>Surname</th>
            <th>Company</th>
        </tr>
        <?php
        foreach ($customerDetails as $customer)
        {
            ?>
            <tr data-function="view" data-controller="customers" data-id="<?php echo $customer->id; ?>">
                <td><?php echo $customer->id; ?></td>
                <td><?php echo $customer->forename; ?></td>
                <td><?php echo $customer->surname; ?></td>
                <td><?php echo $customer->company; ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
</section>