<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MainController extends Controller
{
    public function home() {

    	$data = [
    		'monthlyQuoteSummary'=>[
    			'rank'=>0,
    			'num_quotes'=>0,
    			'average_value'=>0,
    			'total_value'=>0
    		],
    		'totalQuoteSummary'=>[
    			'rank'=>0,
    			'num_quotes'=>0,
    			'average_value'=>0,
    			'total_value'=>0
    		],
    		'quoteDetails'=>[],
    		'customerDetails'=>[]
    	];
    	return view('pages.dashboard', $data);
    }
}
