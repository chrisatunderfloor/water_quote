<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manifold extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function quote()
    {
    	return $this->belongsTo(Quote::class);
    }

    public function room()
    {
    	return $this->hasMany(Room::class);
    }

    public function wiring_centre()
    {
    	return $this->hasMany(WiringCentre::class);
    }
}
