<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    protected $fillable = [
        'quantity',
        'price',
        'unit_price',
    ];

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
