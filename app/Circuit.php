<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circuit extends Model
{
    protected $fillable = [
    	'circuit_number',
        'circuit_area',
        'floor_type',
        'floor_construction_id',
        'fixing_method_id',
        'pipe_length',
        'pipe_spacing',
        'pipe_size'
    ];

    public function room()
    {
    	return $this->belongsTo(Room::class);
    }
}
