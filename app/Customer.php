<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
    	'title',
        'forename',
        'surname',
        'name',
        'company',
        'address1',
        'address2',
        'address3',
        'town',
        'county',
        'postcode',
        'telephone',
        'mobile',
        'email'
    ];

    public function quote()
    {
        return $this->hasMany(Quote::class);
    }
}
