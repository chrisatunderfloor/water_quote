<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
    	'quoteRef',
        'owner_id',
        'customer_id',
        'notes',
        'heat_source',
        'discount_percentage',
        'status',
        'parent_id',
        'total_value',
        'discount_value'
    ];
}
