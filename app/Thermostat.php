<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thermostat extends Model
{
    protected $fillable = [
    	'name',
        'code',
        'price',
        'size',
        'unit',
        'meta',
        'product_types_id'
    ];
}
