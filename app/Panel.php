<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panel extends Model
{
    protected $fillable = [
    	'type',
        'thickness',
        'centres',
        'quantity'
    ];

    public function room()
    {
    	return $this->belongsTo(Room::class);
    }
}
