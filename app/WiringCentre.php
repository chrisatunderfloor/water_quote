<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WiringCentre extends Model
{
    protected $fillable = [
    	'code',
        'name',
        'product_id',
        'manifold_id',
        'quantity'
    ];
}
