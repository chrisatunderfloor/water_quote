const elixir = require('laravel-elixir');

require('laravel-elixir-sass-compass');
require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {

	mix.compass("style.scss",'public/css/',{
		style: "compressed",
		sass: "./resources/assets/sass"
	});

    // mix.scripts([
    // 	'vendor/vue.min.js',
    // 	'vendor/vue-resource.min.js',
    // ])

    mix.scripts([
    	'app.js',
        'bootstrap.js',
    	'button-widget.js',
    ], 'public/js/app.js')

});
